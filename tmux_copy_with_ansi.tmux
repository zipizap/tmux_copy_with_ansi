#!/usr/bin/env bash

CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
SCRIPTS_DIR="${CURRENT_DIR}/scripts"


tmux bind-key   -t vi-copy M-Enter     copy-pipe "$SCRIPTS_DIR/tmux_copy_with_ansi.sh #{pane_id}"
  # NOTE: 
	#		This was tested on tmux 2.1 and should work also with tmux <= 2.3
	#		For tmux >= 2.4, at least the "vi-copy" needs adaptations, and probably other tmux commands 
	#		See https://github.com/tmux/tmux/blob/master/CHANGES
