
tmux_copy_with_ansi
=========

Tmux plugin to copy lines *including ANSI characters* (colors, formatting, ...) into socket listening 127.0.0.1:8377 (where I have [Clipper](https://github.com/wincent/clipper) listening to copy-into-clipboard)

Installing
----------

### Via TPM (recommended)

The easiest way to install `tmux_copy_with_ansi` is via the [Tmux Plugin
Manager](https://github.com/tmux-plugins/tpm).

1.  Add plugin to the list of TPM plugins in `~/.tmux.conf` - add the following line near the end of the file

    ``` tmux
    set -g @plugin 'zipizap/tmux_copy_with_ansi'
    ```

2.  Inside tmux, hit <kbd>prefix</kbd>–<kbd>I</kbd> and it will install `tmux_copy_with_ansi` for the 
		first-time (git clone, and load into tmux config).  You should now be able to use 
		`tmux_copy_with_ansi` immediately.

3.  Whenever there is a newer version of the plugin, to update just hit <kbd>prefix</kbd>–<kbd>U</kbd>.

### Manual Installation

1.  Clone the repository

    ``` sh
    $ git clone https://github.com/zipizap/tmux_copy_with_ansi ~/tmux_copy_with_ansi
    ```

2.  In the bottom of `~/.tmux.conf`, if there is not a line `run '~/.tmux/plugins/tpm/tpm'` then add the following line:

    ``` tmux
    run-shell ~/tmux_copy_with_ansi/tmux_copy_with_ansi.tmux
    ```

3.  Reload the `tmux` environment

    ``` sh
    # type this inside tmux
    $ tmux source-file ~/.tmux.conf
    ```

You should now be able to use `tmux_copy_with_ansi` immediately.


Usage
----------
Inside tmux, at any time, go into Copy-mode...

  <kbd>Ctrl</kbd>-<kbd>[</kbd> 

Select an entire block-of-lines using <kbd>V</kbd> (you should use "V" to assure that only entire lines are selected: from first-character, up to last \n of the line)

  <kbd>V</kbd>  ..make selection of entire lines.. 

When selection is done, hit

  <kbd>Alt</kbd>-<kbd>Enter</kbd> 

And the selected text, will be copied - including ANSI characters - to socket 127.0.0.1:8377tcp (where I have [Clipper](https://github.com/wincent/clipper) listening)


Notes
----------
The plugin copies the selected-lines (with ANSI) into socket 127.0.0.1:8377 because that's where I have [Clipper](https://github.com/wincent/clipper) listening. Clipper in turn puts that into the X-clipboard. This is very handy, because with this setup I can install tmux with this puglin in a remote-VM, and then connect to the remote-tmux with remote-port-forwarding: `ssh -R 127.0.0.1:8377:127.0.0.1:8377 myUser@remoteVm` so that what is copied in the remote-tmux (Alt-Enter) is remote-forwarded into my laptop Clipper and X-clipboard running in my laptop

In any case, it is very easy to change this plugin to make it pipe the text (with ANSI) into any other program (instead of 127.0.0.1:8377) - have a look at scripts/tmux_copy_with_ansi.sh [near the end](https://github.com/zipizap/tmux_copy_with_ansi/blob/2bfb5ae38513ce2fda639cb51b4451e423935060/scripts/tmux_copy_with_ansi.sh#L128), where it says "HACK THE LINES BELLOW" :)




