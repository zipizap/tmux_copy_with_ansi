#!/usr/bin/env bash

### Set up logging
LOG__FILE=/tmp/$(basename $0).log
# Redirect all stdout/stderr of this script, to a logfile. Its a bit primitive, but usefull for debugging
# See https://stackoverflow.com/a/11229532/259192
exec > $LOG__FILE 2>&1
# This logme function helps abstract, in case someday the exec/stdout above needs to be avoided
function logme { 
	#echo $@ >> LOG__FILE
	echo $@ 
}



logme "------------- $(date -Iseconds) -------------------------------------------------------------------"
# Set vars
PANE_ID=$1
logme "PANE_ID: $PANE_ID"
[[ "$PANE_ID" =~ ^%[[:digit:]]+$ ]] || { echo "Unrecognized PANE_ID = $PANE_ID"; exit 1; }

TMP_DIR=$(mktemp -d)
logme "TMP_DIR: $TMP_DIR"
SELECTION_WITHOUT_ANSI__FILE="$TMP_DIR/selection_without_ansi.txt"
SELECTION_WITH_ANSI__FILE="$TMP_DIR/selection_with_ansi.txt"
FULL_PANE_HISTORY_WITH_ANSI__FILE="$TMP_DIR/full_pane_history_with_ansi.txt"
FULL_PANE_HISTORY_WITHOUT_ANSI__FILE="$TMP_DIR/full_pane_history_without_ansi.txt"



### save selection to SELECTION_WITHOUT_ANSI_FILE
# ie, save all stdin into SELECTION_WITHOUT_ANSI_FILE
cat > $SELECTION_WITHOUT_ANSI__FILE
logme "Saved SELECTION_WITHOUT_ANSI__FILE [ $(wc -l $SELECTION_WITHOUT_ANSI__FILE | cut -f1 -d' ') lines ]"


### save entire pane with ansi, to FULL_PANE_HISTORY_WITH_ANSI__FILE
tmux capture-pane -ep -S - -E - -t $PANE_ID > $FULL_PANE_HISTORY_WITH_ANSI__FILE
logme "Saved FULL_PANE_HISTORY_WITH_ANSI__FILE [ $(wc -l $FULL_PANE_HISTORY_WITH_ANSI__FILE | cut -f1 -d' ') lines ]"


### save entire pane without ansi, to FULL_PANE_HISTORY_WITHOUT_ANSI__FILE
tmux capture-pane  -p -S - -E - -t $PANE_ID > $FULL_PANE_HISTORY_WITHOUT_ANSI__FILE
logme "Saved FULL_PANE_HISTORY_WITHOUT_ANSI__FILE [ $(wc -l $FULL_PANE_HISTORY_WITHOUT_ANSI__FILE | cut -f1 -d' ') lines ]"



### Detect inside FULL_PANE_HISTORY_WITHOUT_ANSI__FILE  the beginning of (the last-complete-match of) SELECTION_WITHOUT_ANSI__FILE 
#   and take note of the FULL_PANE_HISTORY__BLOCK_HEADER_LineNr (lineNumber-of-block-start, inside FULL_PANE_HISTORY_WITHOUT_ANSI__FILE)

  #-- full_pane_history_without_ansi.txt --
  #  ...
  #  13 paulo@redBox:~$ #start here              << fake start
  #  14 paulo@redBox:~$
  #  15 paulo@redBox:~$ #start here              |                 << FULL_PANE_HISTORY__BLOCK_HEADER_LineNr=15
  #  16 paulo@redBox:~$                          | the real block that 
  #  17 paulo@redBox:~$                          | needs to be matched
  #  18 paulo@redBox:~$                          |
  #  19 paulo@redBox:~$ #end here                |
  #  ...

  #-- selection_without_ansi.txt --
  #paulo@redBox:~$ #start here       |      << SEL_BLOCK_HEADER_LINE
  #paulo@redBox:~$                   |
  #paulo@redBox:~$                   |SEL_BLOCK
  #paulo@redBox:~$                   |
  #paulo@redBox:~$ #end here         |   
  #                                        SEL_BLOCK_SIZE=5

SEL_BLOCK_SIZE=$(wc -l $SELECTION_WITHOUT_ANSI__FILE | cut -f1 -d' ')
  #5
logme "SEL_BLOCK_SIZE: $SEL_BLOCK_SIZE"

SEL_BLOCK_HEADER_LINE=$(head -1 $SELECTION_WITHOUT_ANSI__FILE)
  #paulo@redBox:~$ #start here

CANDIDATES_LineNrs=$(cat $FULL_PANE_HISTORY_WITHOUT_ANSI__FILE | grep --line-number --fixed-strings -e "$SEL_BLOCK_HEADER_LINE"  | cut -f1 -d: | tac | tr '\n' ' ')
  #15 13   (last-match ... first-match)
  #        These are the lineNrs of lines in FULL_PANE_HISTORY_WITHOUT_ANSI__FILE that match SEL_BLOCK_HEADER_LINE
  #        So these are the lineNrs of the candidadate-lines of FULL_PANE_HISTORY_WITH_ANSI__FILE that might be the start of a valid SEL_BLOCK 
  #        (we still dont know which from these, if any, will really match the entire block)
logme "CANDIDATES_LineNrs: $CANDIDATES_LineNrs"

sel_block_size_minus_one=$((SEL_BLOCK_SIZE - 1))
FULL_PANE_HISTORY__BLOCK_HEADER_LineNr=0
for a_candidate_lineNr in $CANDIDATES_LineNrs; do
  #15
  logme "__ processing a_candidate_lineNr: $a_candidate_lineNr"
  diff -q $SELECTION_WITHOUT_ANSI__FILE <(sed -n "$a_candidate_lineNr,+$sel_block_size_minus_one"p $FULL_PANE_HISTORY_WITHOUT_ANSI__FILE) 
  if [[ $? -eq 0 ]]; then 
    # We found a match!!
    # The SEL_BLOCK was found in FULL_PANE_HISTORY_WITHOUT_ANSI__FILE starting at line a_candidate_lineNr
    # Save the line-nr into FULL_PANE_HISTORY__BLOCK_HEADER_LineNr and get out of for-loop
    FULL_PANE_HISTORY__BLOCK_HEADER_LineNr=$a_candidate_lineNr
    logme "Match found at line $FULL_PANE_HISTORY__BLOCK_HEADER_LineNr"
    break
  fi
done
  #At this point, we either have 
  #    FULL_PANE_HISTORY__BLOCK_HEADER_LineNr==0 , meaning that no match was found, so we just exit now. 
  #                                                This should never happen, if it does then it might probably be a bug in this program...
  #
  #    FULL_PANE_HISTORY__BLOCK_HEADER_LineNr==x , meaning that a match was found, starting at line x
[[ $FULL_PANE_HISTORY__BLOCK_HEADER_LineNr -eq 0 ]] && { 
  logme "This is very strange... The SEL_BLOCK was not found inside FULL_PANE_HISTORY_WITHOUT_ANSI__FILE - no match was found. Look at this more carefully, it might be a bug in this program... TMP_DIR=$TMP_DIR   Aborting now..."
  exit 255
}


### Now that we know:
#      FULL_PANE_HISTORY__BLOCK_HEADER_LineNr   - the line-nr in FULL_PANE_HISTORY_WITHOUT_ANSI__FILE where the BLOCK starts
#      SEL_BLOCK_SIZE                           - nr of lines that form the block, the size of block in lines
#
# We also know (well, assume with great confidence) that both
#      FULL_PANE_HISTORY_WITH_ANSI__FILE and FULL_PANE_HISTORY_WITHOUT_ANSI__FILE
# have the same line-numbers, ie, line-x on one file is the same as line-x in the other file (except for possible ansi-characters that should not change the line-numbering)
# so we conclude that we can cut the BLOCK form the FULL_PANE_HISTORY_WITH_ANSI__FILE by using the FULL_PANE_HISTORY__BLOCK_HEADER_LineNr and SEL_BLOCK_SIZE that we calculated from FULL_PANE_HISTORY_WITHOUT_ANSI__FILE
#
# So we can now go to FULL_PANE_HISTORY_WITH_ANSI__FILE and start cutting from the line FULL_PANE_HISTORY__BLOCK_HEADER_LineNr and cut SEL_BLOCK_SIZE lines, to get the BLOCK including ansi-characters
sed -n "$FULL_PANE_HISTORY__BLOCK_HEADER_LineNr,+$sel_block_size_minus_one"p $FULL_PANE_HISTORY_WITH_ANSI__FILE > $SELECTION_WITH_ANSI__FILE
logme "Saved SELECTION_WITH_ANSI__FILE [ $(wc -l $SELECTION_WITH_ANSI__FILE | cut -f1 -d' ') lines ]"
  # At this point, we have saved in file SELECTION_WITH_ANSI__FILE the text with ansi-characters that we want!

### We can now send the content of SELECTION_WITH_ANSI__FILE to a listening port 
#   Alternatively we could send the content of SELECTION_WITH_ANSI__FILE to any other program: just HACK THE LINES BELLOW
SOCKET_IPv4=127.0.0.1
SOCKET_TCP_PORT=8377
cat $SELECTION_WITH_ANSI__FILE | nc -4 $SOCKET_IPv4 $SOCKET_TCP_PORT
logme "Sent to $SOCKET_IPv4:$SOCKET_TCP_PORT"
tmux display-message "Selected text (including ANSI) sent to $SOCKET_IPv4:$SOCKET_TCP_PORT"

echo -e "\n\n\n\n\n\n\n\n\n\n"
exit 0

